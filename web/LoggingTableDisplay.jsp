<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Exercise 01</title>
</head>
<body>
<style>
    table#accessLogTable tr:nth-child(even) {
        background-color: crimson;
    }
</style>
<table id="accessLogTable">

    <form action="./question1/new" method="post">Name: <br>
        <input type="text" name="name">
        <br>
        Description: <br>
        <input type="text" name="description"><br>
        <br>
        <input type="submit" value="Submit">
    </form>


    <c:forEach items="${accessLogs}" var="accessLogs">
        <tr>
            <td>${accessLogs.id}</td>
            <td>${accessLogs.name}</td>
            <td>${accessLogs.description}</td>
            <td>${accessLogs.currenttime}</td>
        </tr>
    </c:forEach>
</table>
Generate a well formed table of AccessLogs here using JSTL/EL.
</body>
</html>
