package ictgradschool.web.simplewebapp.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DBConnection {

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection createConnection() throws SQLException {

        Properties props = loadProperties();

        return DriverManager.getConnection(props.getProperty("url"), props);

    }

    private static Properties loadProperties() {

        try (InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("jdbc.properties")) {

            Properties props = new Properties();
            props.load(in);
            return props;

        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

}
