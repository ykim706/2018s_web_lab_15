package ictgradschool.web.simplewebapp.inclassexample;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class InClassExampleServlet extends HttpServlet {

    /**
     * In real life, this would come from a database / DAO...
     */
    public static List<CatArticle> getArticles() {

        List<CatArticle> articles = new ArrayList<>();

        articles.add(new CatArticle("Crazy Cheetah", "./images/cheetah.jpg", "Persian malkin british shorthair. Malkin. Panther russian blue siamese for siberian and mouser. Balinese malkin birman tiger and puma. Mouser russian blue for lion ocelot grimalkin. Bombay turkish angora and scottish fold maine coon havana brown american shorthair siamese. Siamese. Donskoy devonshire rex and thai munchkin, so scottish fold. Bombay persian. Burmese abyssinian."));
        articles.add(new CatArticle("Lovely Lion", "./images/lion.jpg", "Cheetah birman and panther, havana brown so bobcat or savannah but havana brown. Munchkin jaguar birman leopard british shorthair or bengal yet donskoy. Havana brown bengal mouser but ragdoll. Malkin siamese. Tiger thai siberian. Siamese ocelot birman. Norwegian forest bombay kitty tiger. Norwegian forest. Maine coon. Cougar kitty and singapura british shorthair, lion so birman. Munchkin. Birman mouser munchkin cougar cheetah and american shorthair."));
        articles.add(new CatArticle("Talented Tiger", "./images/tiger.jpg", "Kitten. Scottish fold. Bombay. Tiger siamese. Bobcat american bobtail egyptian mau russian blue american bobtail, for devonshire rex. British shorthair russian blue maine coon sphynx yet abyssinian and devonshire rex. Sphynx american bobtail but thai lion for havana brown, cougar yet devonshire rex. Donskoy puma so munchkin bobcat and malkin malkin turkish angora."));
        articles.add(new CatArticle("Fluffy Feline", "./images/feline.jpg", "Russian blue. Turkish angora bengal. Cougar egyptian mau yet devonshire rex siberian. Havana brown ragdoll scottish fold. American bobtail lynx donskoy. British shorthair birman bobcat and russian blue but cheetah cornish rex. Bengal puma. Russian blue bengal or cougar. Malkin thai."));
        articles.add(new CatArticle("Powerful Panther", "./images/panther.jpg", "Sphynx grimalkin. Bobcat. Singapura balinese egyptian mau burmese. Bobcat american shorthair birman russian blue and siberian. Cheetah. Puma bombay ocicat. Munchkin balinese balinese but thai. Manx."));
        articles.add(new CatArticle("Wonderful Wildcat", "./images/wildcat.jpg", "Tiger ragdoll or tiger jaguar but siberian bengal. Cougar birman bombay. Maine coon mouser, munchkin. Abyssinian persian. Lion havana brown."));

        return articles;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PrintWriter out = resp.getWriter();

        out.println("<!DOCTYPE html>");
        out.println("<html lang=\"en\">");
        out.println("<head>");
        out.println("<meta charset=\"UTF-8\">");
        out.println("<title>Really Tedious Cat Article Page</title>");
        out.println("</head>");
        out.println("<body>");

        // TODO This...
        List<CatArticle> articles = getArticles();
        for (CatArticle article : articles) {

            out.println("<img src=\"" + article.getImage() + "\">");

        }


        out.println("</body>");
        out.println("</html>");

    }
}
