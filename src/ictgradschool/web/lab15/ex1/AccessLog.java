package ictgradschool.web.lab15.ex1;

import java.io.Serializable;
import java.sql.Timestamp;

public class AccessLog implements Serializable {

    private int id;
    private String name;
    private String description;
    private Timestamp currenttime;

    public AccessLog(int id, String name, String description, Timestamp currenttime) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.currenttime = currenttime;
    }

    public AccessLog() {
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Timestamp getCurrenttime() {
        return currenttime;
    }

    public void setId(int Id) {
        id = Id;
    }

    public void setName(String Name) {
        name = Name;
    }

    public void setDescription(String Description) {
        description = Description;
    }

    public void setCurrenttime(Timestamp Currenttime) {
        currenttime = Currenttime;
    }

}
