package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

public class LoggingTableNewEntry extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Retrieve parameters and store new entries in the database
        // TODO: Redirect back to the LoggingTable
        try (Connection connection = DBConnection.createConnection()) {

            AccessLogDAO dao = new AccessLogDAO(connection);
            String name = request.getParameter("name");
            String description = request.getParameter("description");
            AccessLog accessLog = new AccessLog();
            accessLog.setName(name);
            accessLog.setDescription(description);
            dao.addAccessLog(accessLog);
            response.sendRedirect("../question1");
        } catch (SQLException e){

        }



    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }
}
