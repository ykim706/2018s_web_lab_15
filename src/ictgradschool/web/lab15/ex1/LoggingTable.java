package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class LoggingTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Retrieve LoggingTable entries and pass them to the LoggingTableDisplay.jsp file

        PrintWriter out = response.getWriter();

        try {
            List<AccessLog> accessLogs = displayAccessLogList(out);

            request.setAttribute("accessLogs", accessLogs);
            request.getRequestDispatcher("LoggingTableDisplay.jsp").forward(request, response);

        } catch (SQLException e) {
            throw new ServletException(e);
        }


    }

    private List<AccessLog> displayAccessLogList(PrintWriter out) throws SQLException {
        try (Connection connection = DBConnection.createConnection()) {
            AccessLogDAO dao = new AccessLogDAO(connection);
            List<AccessLog> accessLogs = dao.allAccessLog();
            return accessLogs;
        }
    }


}
