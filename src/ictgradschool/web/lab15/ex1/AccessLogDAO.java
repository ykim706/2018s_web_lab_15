package ictgradschool.web.lab15.ex1;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AccessLogDAO {
    private final Connection connect;

    public AccessLogDAO(Connection connect) {
        this.connect = connect;
    }

    public List<AccessLog> allAccessLog() throws SQLException {
        List<AccessLog> accessLogs = new ArrayList<AccessLog>();

        try (Statement statement = connect.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery("SELECT * FROM access_log")) {
                while (resultSet.next()) {
                    AccessLog accessLog = getAccessLogFromResultSet(resultSet);
                    accessLogs.add(accessLog);
                }
            }
        }
        return accessLogs;
    }

    private AccessLog getAccessLogFromResultSet(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String name = resultSet.getString("name");
        String description = resultSet.getString("description");
        Timestamp currenttime = resultSet.getTimestamp("currenttime");

        return new AccessLog(id, name, description, currenttime);
    }

    public void addAccessLog(AccessLog a) throws SQLException {
        try (PreparedStatement preparedStatement = connect.prepareStatement
                ("INSERT INTO access_log(id, name, description) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setInt(1, a.getId());
            preparedStatement.setString(2, a.getName());
            preparedStatement.setString(3, a.getDescription());

            int numRows = preparedStatement.executeUpdate();
            System.out.println(numRows + " rows added");

            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            int auto_id = resultSet.getInt(1);
        }
    }
}
