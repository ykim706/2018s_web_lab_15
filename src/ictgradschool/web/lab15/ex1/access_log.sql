DROP TABLE IF EXISTS access_log;
CREATE TABLE IF NOT EXISTS access_log(
id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(100),
description VARCHAR(1000),
currenttime TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

INSERT INTO access_log(id, name, description) VALUES
(111, 'Versace', 'High end brand'),
(333, 'Zara','SPA brand'),
(222, 'H&M', 'SPA brand');